package ru.t1.fpavlov.tm.api.model;

import java.util.Date;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IHasCreated {

    Date getCreated();

    void setCreated(final Date created);

}
