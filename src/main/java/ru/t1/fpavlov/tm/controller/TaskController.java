package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.controller.ITaskController;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void create() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task item = taskService.create(name, description);
        if (item == null) {
            System.out.println("\tError!");
        } else {
            System.out.println("\tOk");
        }
    }

    @Override
    public void clear() {
        System.out.println("Clear task");
        taskService.clear();
        System.out.println("\tOk");
    }

    @Override
    public void showAll() {
        System.out.println("Available sort:");
        System.out.println(Arrays.toString(Sort.displayValues()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> items = taskService.findAll(sort);
        renderTasks(items);
    }

    @Override
    public void showAllByProjectId() {
        System.out.println("Enter the project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Available sort:");
        System.out.println(Arrays.toString(Sort.displayValues()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> items = this.taskService.findAllByProjectId(projectId);
        renderTasks(items);
    }

    @Override
    public void removeById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        final Task item = taskService.removeById(itemId);
        if (item == null) {
            System.out.format("Error! Task with id(%s)." +
                    "Wasn't found%n", itemId);
        } else {
            System.out.format("Task with id(%s). Was " +
                    "removed successfully%n", itemId);
        }
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Task item = taskService.removeByIndex(itemIndex - 1);
        if (item == null) {
            System.out.format("Error! Task with index(%d)." +
                    "Wasn't found%n", itemIndex);
        } else {
            System.out.format("Task with index(%d). Was " +
                    "removed successfully%n", itemIndex);
        }
    }

    @Override
    public void showById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        final Task item = taskService.findById(itemId);
        if (item == null) {
            System.out.format("Error! Task with id(%s)." +
                    "Wasn't found%n", itemId);
        } else {
            System.out.format("Task with id(%s). Was " +
                    "found successfully%n%s%n", itemId, item);
        }
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Task item = taskService.findByIndex(itemIndex - 1);
        if (item == null) {
            System.out.format("Error! Task with index(%d)." +
                    "Wasn't found%n", itemIndex);
        } else {
            System.out.format("Task with index(%d). Was " +
                    "found successfully%n%s%n", itemIndex, item);
        }
    }

    @Override
    public void updateById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        final Task item = taskService.updateById(itemId, name, description);
        if (item == null) {
            System.out.format("Error! Task with id(%s)." +
                    "Wasn't found%n", itemId);
        } else {
            System.out.format("Task with id(%s). Was " +
                    "updated successfully%n%s%n", itemId, item);
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        final Task item = taskService.updateByIndex(itemIndex - 1, name, description);
        if (item == null) {
            System.out.format("Error! Task with id(%s)." +
                    "Wasn't found%n", itemIndex);
        } else {
            System.out.format("Task with index(%s). Was " +
                    "updated successfully%n%s%n", itemIndex, item);
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task item = this.taskService.changeStatusById(itemId, status);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger() - 1;
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task item = this.taskService.changeStatusByIndex(itemIndex, status);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        final Task item = this.taskService.changeStatusById(itemId, Status.IN_PROGRESS);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger() - 1;
        final Task item = this.taskService.changeStatusByIndex(itemIndex, Status.IN_PROGRESS);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void completeById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        final Task item = this.taskService.changeStatusById(itemId, Status.COMPLETED);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void completeByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger() - 1;
        final Task item = this.taskService.changeStatusByIndex(itemIndex, Status.COMPLETED);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    public void renderTasks(final List<Task> items) {
        int index = 1;
        System.out.println("Projects list:");
        for (final Task item : items) {
            if (item == null) continue;
            System.out.format("|%2d%s%n", index, item);
            index++;
        }
        System.out.println("\tOk");
    }

}
