package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.IProjectTaskService;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/**
 * Created by fpavlov on 26.11.2021.
 */
public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        final Project project = this.projectRepository.findById(projectId);
        if (project == null) return;
        final Task task = this.taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        final Project project = this.projectRepository.findById(projectId);
        if (project == null) return;
        final List<Task> tasks = this.taskRepository.findAllByProjectId(projectId);
        for (final Task item : tasks) this.taskRepository.remove(item);
        this.projectRepository.removeById(projectId);
    }

    @Override
    public void unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        final Project project = this.projectRepository.findById(projectId);
        if (project == null) return;
        final Task task = this.taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

}
